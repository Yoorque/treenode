//
//  ViewController.swift
//  TreeTable
//
//  Created by Dusan Juranovic on 11/24/19.
//  Copyright © 2019 Dusan Juranovic. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var treeArray: [Player] = []
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        addDummyData()
        registerCell()
    }
    
    func addDummyData() {
        //parent 1
        let parentOne = Player(value: "Nikola")
        let child1 = Player(value: "Marija")
        let child2 = Player(value: "Jovana")
        let child3 = Player(value: "Ana")
        let grandchild = Player(value: "Ivana")
        child1.addChild(grandchild)
        parentOne.addChild(child1)
        parentOne.addChild(child2)
        parentOne.addChild(child3)
        
        //parent 2
        let parentTwo = Player(value: "Bojan")
        let child4 = Player(value: "Uroš")
        let child5 = Player(value: "Igor")
        let grandchild1 = Player(value: "Mali Radojica")
        let grandchild2 = Player(value: "Nikolica s Prikolicom")
        let grandchild3 = Player(value: "Boško Buha")
        child4.addChild(grandchild1)
        child5.addChild(grandchild2)
        child5.addChild(grandchild3)
        parentTwo.addChild(child4)
        parentTwo.addChild(child5)
        
        treeArray.append(parentOne)
        treeArray.append(parentTwo)
    }
}

//MARK: - ButtonActionDelegate
extension ViewController: ButtonActionDelegate {
    
    func buttonTappedAt(section: Int, row: Int) {
        performSegue(withIdentifier: "Child", sender: (section, row))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ChildViewController {
            let (section, row) = sender as! (Int, Int)
            destination.view.backgroundColor = [UIColor.red, UIColor.blue, UIColor.yellow].randomElement()
            destination.childName.text = treeArray[section].children![row].value
            destination.childName.textColor = [UIColor.white, UIColor.black].randomElement()
            destination.child = treeArray[section].children![row]
            present(destination, animated: true, completion: nil)
        }
    }
}

//MARK: - UITableViewDelegate , UITableViewDataSource
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func registerCell() {
        tableView.register(NodeCell.self, forCellReuseIdentifier: "nodeCell")
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return treeArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if treeArray[section].opened == true {
            return treeArray[section].children!.count + 1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "playerCell", for: indexPath)
            cell.textLabel?.text = treeArray[indexPath.section].value
            cell.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7)
            cell.indentationWidth = 0
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "nodeCell", for: indexPath) as? NodeCell else {
                return UITableViewCell()
            }
            cell.titleLabel?.text = treeArray[indexPath.section].children?[indexPath.row - 1].value
            cell.positionInTableView = (indexPath.section, indexPath.row - 1)
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if treeArray[indexPath.section].opened == true {
                treeArray[indexPath.section].opened = false
                let sections = IndexSet(integer: indexPath.section)
                tableView.reloadSections(sections, with: .automatic)
            } else {
                treeArray[indexPath.section].opened = true
                let sections = IndexSet(integer: indexPath.section)
                tableView.reloadSections(sections, with: .automatic)
            }
            toggleOpenStateOfAllCellsAccordingTo(treeArray[indexPath.section])
        } else {
            print("Cell tapped", treeArray[indexPath.section].children![indexPath.row - 1].value)
        }
        
    }
    
    func toggleOpenStateOfAllCellsAccordingTo(_ node: Player) {
        if node.opened == true {
            for parent in treeArray {
                if parent != node {
                    parent.opened = false
                }
            }
        }
        tableView.reloadData()
    }
}
