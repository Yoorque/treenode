//
//  ChildViewController.swift
//  TreeTable
//
//  Created by Dusan Juranovic on 11/25/19.
//  Copyright © 2019 Dusan Juranovic. All rights reserved.
//

import UIKit

class ChildViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var childName: UILabel!
    var child: Player?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
}

extension ChildViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if child?.children?.count != 0 {
            return child!.children!.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "childCell", for: indexPath)
        if child?.children?.count != 0 {
            cell.textLabel?.text = child?.children?[indexPath.row].value
        } else {
            cell.textLabel?.text = "No children"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UILabel()
        view.text = "Children:"
        return view
    }
}
