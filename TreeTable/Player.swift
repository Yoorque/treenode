//
//  Player.swift
//  TreeTable
//
//  Created by Dusan Juranovic on 11/24/19.
//  Copyright © 2019 Dusan Juranovic. All rights reserved.
//

import Foundation
class Player: Sequence {
    var value: String
    weak var parent: Player?
    var children: [Player]? = []
    var opened = Bool()

    init(value:String) {
        self.value = value
    }

    __consuming func makeIterator() -> AnyIterator<Player> {
        var index = children!.startIndex
        
        return AnyIterator {
            if (index < self.children!.endIndex) {
                let child = self.children![index]
              index = self.children!.index(after: index)
              return child
            } else { return nil }
        }
    }
    
    func addChild(_ node: Player) {
        children?.append(node)
        node.parent = self
    }
    
//    func displayChildrenOf(_ node: Player) {
//        guard node.children != nil else {
//            print("I am \(node.value) and I have no children")
//            return
//        }
//        print("I am \(node.value) and these are my \(node.children!.count) children")
//
//        for child in node.children! {
//            print(" ", child.value)
//            if child.children?.count != 0 {
//                displayChildrenOf(child)
//                print("\n")
//            }
//        }
//    }
}

extension Player: Equatable {
    static func == (lhs: Player, rhs: Player) -> Bool {
        return lhs.value == rhs.value
    }
    
    
}


