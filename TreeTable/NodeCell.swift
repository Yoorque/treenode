//
//  NodeCell.swift
//  TreeTable
//
//  Created by Dusan Juranovic on 11/24/19.
//  Copyright © 2019 Dusan Juranovic. All rights reserved.
//

import UIKit
protocol ButtonActionDelegate: class {
    func buttonTappedAt(section: Int, row: Int)
}

class NodeCell: UITableViewCell {
    weak var delegate: ButtonActionDelegate!
    var titleLabel: UILabel!
    var stackView: UIStackView!
    var openButton: UIButton!
    var isSelectedCell: Bool = false
    var positionInTableView: (Int,Int)!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupTitleLabel()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
    }
    
    func setupTitleLabel() {
        self.titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.titleLabel.textColor = .white
        self.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        self.addSubview(titleLabel)
        
        self.openButton = UIButton(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        self.openButton.translatesAutoresizingMaskIntoConstraints = false
        self.openButton.setTitle("Open", for: .normal)
        self.openButton.titleLabel?.textColor = .blue
        self.openButton.isUserInteractionEnabled = true
        self.openButton.isEnabled = true
        self.openButton.addTarget(self, action: #selector(openButtonTapped(_:)), for: .touchUpInside)
        self.addSubview(openButton)
    }
    
    func setupConstraints() {
        let hConst = NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[label]-20-[button(80)]-10-|", options: [], metrics: nil, views: ["label":titleLabel!, "button":openButton!])
        let vLConst = NSLayoutConstraint.constraints(withVisualFormat: "V:|[label]|", options: [], metrics: nil, views: ["label":titleLabel!])
        
        NSLayoutConstraint(item: openButton!, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: openButton!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40).isActive = true
        NSLayoutConstraint.activate(hConst)
        NSLayoutConstraint.activate(vLConst)
        self.updateConstraints()
        self.layoutIfNeeded()
    }
    
    @objc func openButtonTapped(_ sender: UIButton) {
        delegate.buttonTappedAt(section: positionInTableView.0, row: positionInTableView.1)
    }
   
}
